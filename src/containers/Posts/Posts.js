import React, {useEffect} from 'react';
import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import {useDispatch, useSelector} from "react-redux";
import PostsItem from "./PostsItem";
import {fetchPosts} from "../../store/actions/postsActions";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Posts = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const posts = useSelector(state => state.posts.posts);
    const loading = useSelector(state => state.posts.postsLoading);

    useEffect(() => {
       dispatch(fetchPosts());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Posts</Typography>
                </Grid>
            </Grid>
            <Grid item container spacing={1} direction="column">
                {loading ? (
                    <Grid container justify="center" alignItems="center" className={classes.progress}>
                        <Grid item>
                            <CircularProgress />
                        </Grid>
                    </Grid>
                ) : posts.map(post => (
                    <PostsItem
                        key={post._id}
                        id={post._id}
                        title={post.title}
                        date={post.datetime}
                        image={post.image}
                        user={post.user.username}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default Posts;