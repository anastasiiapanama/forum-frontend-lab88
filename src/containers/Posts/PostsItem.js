import React from 'react';
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Link} from "react-router-dom";
import {Card, CardContent, CardMedia, IconButton} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import {apiURL} from "../../config";

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '5px',
        marginBottom: '8px'
    },
    contentBlock: {
        display: 'flex',
    },
    cover: {
        width: '150px',
        height: '150px',
        overflow: 'hidden',
        borderRadius: '4px'
    },
    content: {
        display: 'flex',
        flexDirection: 'column'
    },
    title: {
        marginBottom: '20px',
        fontSize: 15
    }
}));

const PostsItem = ({id, title, date, image, user}) => {
    const classes = useStyles();

    if (image) {
        image = <CardMedia
            className={classes.cover}
            image={apiURL + '/' + image}
            title={title}
        />
    } else {
        image = <IconButton>
            <ChatBubbleIcon color="primary" style={{ fontSize: 70 }}/>
        </IconButton>
    }

    return (
        <Card className={classes.root}>
            <div className={classes.contentBlock}>
                {image}
                <CardContent className={classes.content}>
                    <Typography component="h6" variant="h6" className={classes.title}>
                        {date} by <strong>{user}</strong>
                    </Typography>
                    <Typography color="textSecondary" component={Link} to={'/posts/' + id}>
                        <strong style={{fontSize: 20}}>{title}</strong>
                    </Typography>
                </CardContent>
            </div>
        </Card>
    );
};

export default PostsItem;