import React from 'react';
import {useDispatch} from "react-redux";
import PostForm from "../../components/PostForm/PostForm";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {createPost} from "../../store/actions/postsActions";

const NewPost = ({history}) => {
    const dispatch = useDispatch();

    const onPostFormSubmit = async postData => {
        await  dispatch(createPost(postData));

        history.push('/');
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item xs>
                <Typography variant="h4">New post</Typography>
            </Grid>
            <Grid item xs>
                <PostForm
                    onSubmit={onPostFormSubmit}
                />
            </Grid>
        </Grid>
    );
};

export default NewPost;