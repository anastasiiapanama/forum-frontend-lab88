import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";

import Grid from "@material-ui/core/Grid";
import {Box, Divider, Paper, TextField} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import {fetchSinglePost} from "../../store/actions/postsActions";
import Button from "@material-ui/core/Button";
import {createComment, fetchCommentsForPost} from "../../store/actions/commentsActions";

const Post = ({match}) => {
    const dispatch = useDispatch();
    const post = useSelector(state => state.posts.singlePost);
    const user = useSelector(state => state.users.user);
    const commentsForPost = useSelector(state => state.comments.commentsForPost);
    const [comment, setComment] = useState('');

    useEffect(() => {
        dispatch(fetchSinglePost(match.params.id));
        dispatch(fetchCommentsForPost(match.params.id));
    }, [dispatch, match.params.id]);

    const onCommentFormSubmit = async e => {
        e.preventDefault();

        await dispatch(createComment({
            comment,
            postId: match.params.id
        }));

        setComment('');
    };

    return post ? (
        <Grid container direction="column" spacing="2">
            <Grid item xs>
                <Typography variant="h5">{post.title}</Typography>
            </Grid>
            {post.image && (
                <Grid item xs>
                    <img src={'http://localhost:8000/' + post.image}
                         alt={post.title} style={{maxWidth: 200, maxHeight: 200}}
                    />
                </Grid>
            )}
            <Grid item xs>
                <Typography variant="body1">{post.description}</Typography>
            </Grid>
            <Grid item xs>
                <Divider/>
            </Grid>
            <Grid item xs>
                <Typography variant="body2">Created at: {post.datetime}</Typography>
            </Grid>
            <Grid item xs>
                <Divider/>
            </Grid>
            <Grid item xs>
                <Typography>Comments</Typography>
            </Grid>
            <Grid item xs container direction="column" spacing={1}>
                {commentsForPost.length === 0 ? (
                    <Typography variant={"body2"}>No comment</Typography>
                ) : commentsForPost.map(comment => (
                    <Grid item key={comment._id}>
                        <Paper component={Box}>
                            <Typography><b>Author: </b>{comment.user.username}</Typography>
                            <Typography>{comment.comment}</Typography>
                        </Paper>
                    </Grid>
                ))}
            </Grid>
            <Grid item xs>
                <Divider/>
            </Grid>
            {user ? (
                <Grid container direction="column" spacing={2} component="form" onSubmit={onCommentFormSubmit}>
                    <Grid item xs>
                        <Typography variant="h6">Add new comment</Typography>
                    </Grid>
                    <Grid item xs>
                        <TextField
                            fullWidth
                            variant="outlined"
                            label="Comment"
                            value={comment}
                            onChange={e => setComment(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs>
                        <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                        >
                            Send comment
                        </Button>
                    </Grid>
                </Grid>
            ) : null}
        </Grid>
    ) : null;
}

export default Post;