import axiosApi from "../../axios-api";
import {NotificationManager} from "react-notifications";

export const FETCH_COMMENTS_FOR_POST_REQUEST = 'FETCH_COMMENTS_FOR_REQUEST';
export const FETCH_COMMENTS_FOR_POST_SUCCESS = 'FETCH_COMMENTS_FOR_SUCCESS';
export const FETCH_COMMENTS_FOR_POST_FAILURE = 'FETCH_COMMENTS_FOR_FAILURE';

export const fetchCommentsForPostRequest = () => ({type: FETCH_COMMENTS_FOR_POST_REQUEST});
export const fetchCommentsForPostSuccess = comments => ({type: FETCH_COMMENTS_FOR_POST_SUCCESS, comments});
export const fetchCommentsForPostFailure = () => ({type: FETCH_COMMENTS_FOR_POST_FAILURE});

export const createComment = commentData => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {Authorization: token}

        await axiosApi.post('/comments', commentData, {headers});

        await dispatch(fetchCommentsForPost(commentData.postId));
    };
};

export const fetchCommentsForPost = postId => {
    return async dispatch => {
        try {
            dispatch(fetchCommentsForPostRequest());
            const response = await axiosApi.get('/comments?postId=' + postId);
            dispatch(fetchCommentsForPostSuccess(response.data));
        } catch (e) {
            dispatch(fetchCommentsForPostFailure());
            NotificationManager.error('Could not fetch comments');
        }
    };
};

