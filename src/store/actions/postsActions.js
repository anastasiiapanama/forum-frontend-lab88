import {NotificationManager} from "react-notifications";
import axiosApi from "../../axios-api";
import {historyPush} from "./historyActions";

export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';

export const FETCH_SINGLE_POST_REQUEST = 'FETCH_SINGLE_POST_REQUEST';
export const FETCH_SINGLE_POST_SUCCESS = 'FETCH_SINGLE_POST_SUCCESS';
export const FETCH_SINGLE_POST_FAILURE = 'FETCH_SINGLE_POST_FAILURE';

export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';

export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const fetchPostsFailure = () => ({type: FETCH_POSTS_FAILURE});

export const fetchSinglePostRequest = () => ({type: FETCH_SINGLE_POST_REQUEST});
export const fetchSinglePostSuccess = post => ({type: FETCH_SINGLE_POST_SUCCESS, post});
export const fetchSinglePostFailure = () => ({type: FETCH_SINGLE_POST_FAILURE});

export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});

export const fetchPosts = () => {
    return async dispatch => {
        try {
            dispatch(fetchPostsRequest());
            const response = await axiosApi.get('/posts');
            dispatch(fetchPostsSuccess(response.data));
        } catch (e) {
            dispatch(fetchPostsFailure());
            NotificationManager.error('Could not fetch posts');
        }
    };
};

export const fetchSinglePost = id => {
    return async dispatch => {
        try {
            dispatch(fetchSinglePostRequest());
            const response = await axiosApi.get('/posts/' + id);
            dispatch(fetchSinglePostSuccess(response.data));
        } catch (e) {
            dispatch(fetchSinglePostFailure());
            NotificationManager.error('Could not fetch posts');
        }
    };
};

export const createPost = postData => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {Authorization: token}

        await axiosApi.post('/posts', postData, {headers});

        dispatch(createPostSuccess());
        dispatch(historyPush('/'));
    };
};