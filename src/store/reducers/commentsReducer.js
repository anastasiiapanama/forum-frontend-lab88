import {
    FETCH_COMMENTS_FOR_POST_FAILURE,
    FETCH_COMMENTS_FOR_POST_REQUEST,
    FETCH_COMMENTS_FOR_POST_SUCCESS
} from "../actions/commentsActions";

const initialState = {
    commentsForPost: [],
    commentsLoading: false
};

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_FOR_POST_REQUEST:
            return {...state, commentsLoading: true};
        case FETCH_COMMENTS_FOR_POST_SUCCESS:
            return {...state, commentsLoading: false, commentsForPost: action.comments};
        case FETCH_COMMENTS_FOR_POST_FAILURE:
            return {...state, commentsLoading: false};
        default:
            return state;
    };
};

export default commentsReducer;