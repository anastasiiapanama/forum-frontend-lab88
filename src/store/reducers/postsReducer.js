import {
    FETCH_POSTS_FAILURE,
    FETCH_POSTS_REQUEST,
    FETCH_POSTS_SUCCESS, FETCH_SINGLE_POST_FAILURE,
    FETCH_SINGLE_POST_REQUEST, FETCH_SINGLE_POST_SUCCESS
} from "../actions/postsActions";

const initialState = {
    posts: [],
    singlePost: {},
    postsLoading: false
};

const postsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_POSTS_REQUEST:
            return {...state, postsLoading: true};
        case FETCH_POSTS_SUCCESS:
            return {...state, postsLoading: false, posts: action.posts};
        case FETCH_POSTS_FAILURE:
            return {...state, postsLoading: false};
        case FETCH_SINGLE_POST_REQUEST:
            return {...state, postsLoading: true};
        case FETCH_SINGLE_POST_SUCCESS:
            return {...state, postsLoading: false, singlePost: action.post};
        case FETCH_SINGLE_POST_FAILURE:
            return {...state, postsLoading: false};
        default:
            return state;
    };
};

export default postsReducer;