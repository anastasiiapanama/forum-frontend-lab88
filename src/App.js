import {Route, Switch} from "react-router-dom";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Posts from "./containers/Posts/Posts";
import Post from "./containers/Post/Post";
import NewPost from "./containers/NewPost/NewPost";

import {Container, CssBaseline} from "@material-ui/core";

const App = () => (
    <>
        <CssBaseline/>
        <header>
            <AppToolbar/>
        </header>
        <main>
            <Container maxWidth="xl">
                <Switch>
                    <Route path="/" exact component={Posts}/>
                    <Route path="/posts/:id" exact component={Post}/>
                    <Route path="/new-post" exact component={NewPost}/>
                    <Route path="/register" component={Register}/>
                    <Route path="/login" component={Login}/>
                </Switch>
            </Container>
        </main>
    </>
);

export default App;
